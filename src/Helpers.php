<?php

namespace Drupal\domain_fields_visualization;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class Helpers {

  use StringTranslationTrait;


  public static function domain_fields_settings($domain = NULL) {
    $domain = \Drupal::service('domain.negotiator');
    if ($domain) {
      $current = $domain->negotiateActiveHostname();
    }

    return \Drupal::state()
      ->get('domain_fields_' . $current);
  }

  public static function _domain_fields_instance_settings($entity_type, $bundle, $field_name, $key, $default_value = 0, $domain = NULL) {
    $settings = self::domain_fields_settings($domain);
    if (isset($settings[$entity_type][$bundle][$field_name][$key])) {
      return $settings[$entity_type][$bundle][$field_name][$key];
    }
    return $default_value;
  }

}
